use sqlx::{self, PgPool};
use std::{fmt, vec, fs::{self, OpenOptions}, io::{Write, self}, error::Error};

#[derive(Debug, Clone, PartialEq)]
pub enum DeviceStateType {
    ON,
    OFF,
}

impl fmt::Display for DeviceStateType {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:?}", self)
    }
}

#[derive(Debug, PartialEq)]
pub struct Device {
    name: String,
    state: DeviceStateType,
    info: String,
}

impl Device {
    pub fn new(name: String, info: String) -> Device {
        Device {
            name,
            state: DeviceStateType::OFF,
            info,
        }
    }

    pub fn on(&mut self) {
        self.state = DeviceStateType::ON
    }

    pub fn off(&mut self) {
        self.state = DeviceStateType::OFF
    }

    pub fn get_info(&self) -> String {
        if self.state == DeviceStateType::ON {
            format!("{}:\n\t{}", self.name, self.info)
        } else {
            format!(
                "{} is turned off. If you want a description, you should enable the socket",
                self.name
            )
        }
    }

    pub fn get_device_state(&self) -> DeviceStateType {
        self.state.clone()
    }
}

#[derive(Debug, PartialEq)]
pub struct Room {
    name: String,
    devices: Vec<Device>,
}

impl Room {
    pub fn new(name: String) -> Room {
        Room {
            name,
            devices: vec![],
        }
    }

    pub fn get_devices(&self) -> Result<Vec<String>, Box<dyn Error>> {
        let mut devices = vec![];
        self
            .devices
            .iter()
            .for_each(|device: &Device| devices.push(device.name.clone()));

        Ok(devices)
    }

    pub fn get_device_by_name(&self, name: String) -> Result<&Device, Box<dyn Error>> {
        let device = self.devices
            .iter()
            .find(|device| device.name == name)
            .unwrap();

        Ok(device)
    }

    pub fn register_new_device(&mut self, device_name: String, info: String) -> Result<&mut Room, Box<dyn Error>> {
        let device = Device::new(format!("{}_{}", self.name, device_name), info);
        self.devices.push(device);
        Ok(self)
    }
}

impl fmt::Display for Room {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{} room:\n\tDevices: {:?}", self.name, self.devices)
    }
}

// SmartHouse - basic structure that allows you to manage devices and rooms
#[derive(Debug, PartialEq)]
pub struct SmartHouse {
    pub name: String,
    pub rooms: Vec<Room>,
}

impl SmartHouse {
    /// Create house
    ///
    /// ```
    /// use smart_house::SmartHouse;
    ///
    /// let house = SmartHouse::new("house".to_string());
    /// let mock_house = SmartHouse { name: "house".to_string(), rooms: vec![] };
    ///
    /// assert_eq!(mock_house, house);
    /// ```
    pub fn new(name: String) -> SmartHouse {
        SmartHouse {
            name,
            rooms: vec![],
        }
    }

    /// Get rooms
    ///
    /// ```
    /// use smart_house::{ SmartHouse, Room };
    ///
    /// let mut house = SmartHouse::new("house".to_string());
    /// let kitchen = Room::new("kitchen".to_string());
    /// house.register_new_room(kitchen);
    /// let rooms = house.get_rooms().unwrap();
    ///
    /// assert!(!rooms.is_empty());
    /// ```
    pub fn get_rooms(&self) -> Result<Vec<String>, Box<dyn Error>> {
        let mut rooms = vec![];
        self
            .rooms
            .iter()
            .for_each(|room| rooms.push(room.name.clone()));
        
        Ok(rooms)
    }

    /// Get rooms by name
    ///
    /// ```
    /// use smart_house::{ SmartHouse, Room };
    ///
    /// let mut house = SmartHouse::new("house".to_string());
    /// let kitchen = Room::new("kitchen".to_string());
    /// let hall = Room::new("hall".to_string());
    /// house
    ///     .register_new_room(kitchen)
    ///     .unwrap()
    ///     .register_new_room(hall);
    /// let kitchen = house.get_room_by_name("kitchen".to_string()).unwrap();
    ///
    /// assert_eq!(&Room::new("kitchen".to_string()), kitchen);
    /// ```
    pub fn get_room_by_name(&self, name: String) -> Result<&Room, Box<dyn Error>> {
        let room = self.rooms.iter().find(|room| room.name == name).unwrap();
        Ok(room)
    }

    /// Register new room
    ///
    /// ```
    /// use smart_house::{ SmartHouse, Room };
    ///
    /// let mut house = SmartHouse::new("house".to_string());
    /// let kitchen = Room::new("kitchen".to_string());
    /// house.register_new_room(kitchen);
    /// let rooms = house.get_rooms().unwrap();
    ///
    /// assert!(!rooms.is_empty());
    /// ```
    pub async fn register_new_room<'a>(&self, pool: &'a PgPool) -> Result<&SmartHouse, Box<dyn Error>> {
        println!("Enter room's name:");
        let mut room_name = String::new();
        let _ = io::stdin().read_line(&mut room_name);
        let _ = sqlx::query("insert into rooms (name) values ($1)")
            .bind(room_name)
            .fetch_one(pool)
            .await?;


        println!("\nadded successfully!\n");
        Ok(self)
    }

    /// Delete room
    ///
    /// ```
    /// use smart_house::{ SmartHouse, Room };
    ///
    /// let mut house = SmartHouse::new("house".to_string());
    /// let kitchen = Room::new("kitchen".to_string());
    /// let hall = Room::new("hall".to_string());
    /// house
    ///     .register_new_room(kitchen)
    ///     .unwrap()
    ///     .register_new_room(hall);
    /// house.delete_room("kitchen".to_string());
    /// let rooms = house.get_rooms().unwrap();
    /// let mock_rooms = vec!["hall".to_string()];
    ///
    /// assert_eq!(mock_rooms, rooms);
    /// ```
    pub fn delete_room(&mut self, room_name: String) -> Result<&mut SmartHouse, Box<dyn Error>> {
        let index = self
            .rooms
            .iter()
            .position(|room| room.name == room_name)
            .unwrap();
        self.rooms.remove(index);
        println!("\n{room_name} removed successfully!\n");
        Ok(self)
    }

    pub fn create_report(&self) {
        let path = "target/report";
        fs::create_dir_all(path).unwrap();

        let mut file = OpenOptions::new()
            .create(true)
            .write(true)
            .truncate(true)
            .open(format!("{path}/report.txt"))
            .unwrap();

        self.get_rooms()
            .unwrap()
            .into_iter()
            .for_each(|room| {
                file.write_all(format!("\n{room}:").as_bytes()).unwrap();
                file.write_all(b"\tDevices:").unwrap();

                let room = self.get_room_by_name(room).unwrap();
                let devices = room.get_devices().unwrap();
                devices.into_iter().for_each(|device| {
                    let Device { name, state, info } = room.get_device_by_name(device).unwrap();
                    file.write_all(format!("\t\t{}:{}. {}", name, state, info).as_bytes()).unwrap();
                });
            });
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn create_house() {
        assert_eq!(
            SmartHouse {
                name: "house".to_string(),
                rooms: vec![],
            },
            SmartHouse::new("house".to_string())
        )
    }

    // #[test]
    // fn get_all_rooms() {
    //     let mut house = SmartHouse::new("My home".to_string());
    //     let kitchen = Room::new("kitchen".to_string());
    //     let hall = Room::new("hall".to_string());

    //     let _ = house
    //         .register_new_room(kitchen)
    //         .unwrap()
    //         .register_new_room(hall);

    //     assert_eq!(vec!["kitchen", "hall"], house.get_rooms().unwrap())
    // }

    // #[test]
    // fn get_room_by_name() {
    //     let mut house = SmartHouse::new("My home".to_string());
    //     let kitchen = Room::new("kitchen".to_string());
    //     let hall = Room::new("hall".to_string());

    //     let _ = house
    //         .register_new_room(kitchen)
    //         .unwrap()
    //         .register_new_room(hall);

    //     assert_eq!(
    //         &Room::new("kitchen".to_string()),
    //         house.get_room_by_name("kitchen".to_string()).unwrap()
    //     )
    // }

    // #[test]
    // fn register_new_room() {
    //     let test_house = SmartHouse {
    //         name: "house".to_string(),
    //         rooms: vec![Room {
    //             name: "kitchen".to_string(),
    //             devices: vec![],
    //         }],
    //     };
    //     let mut house = SmartHouse::new("house".to_string());
    //     let _ = house.register_new_room(Room {
    //         name: "kitchen".to_string(),
    //         devices: vec![],
    //     });

    //     assert_eq!(test_house, house)
    // }

    #[test]
    fn get_all_devices() {
        let mut kitchen = Room::new("kitchen".to_string());
        let _ = kitchen
            .register_new_device("socket1".to_string(), "Model A".to_string()).unwrap()
            .register_new_device("socket2".to_string(), "Model A".to_string());

        assert_eq!(
            vec!["kitchen_socket1", "kitchen_socket2"],
            kitchen.get_devices().unwrap()
        )
    }

    #[test]
    fn get_device_by_name() {
        let mut kitchen = Room::new("kitchen".to_string());
        let _ = kitchen
            .register_new_device("socket1".to_string(), "Model A".to_string()).unwrap()
            .register_new_device("socket2".to_string(), "Model A".to_string());

        assert_eq!(
            &Device::new("kitchen_socket1".to_string(), "Model A".to_string()),
            kitchen.get_device_by_name("kitchen_socket1".to_string()).unwrap()
        )
    }

    #[test]
    fn register_new_device() {
        let test_room = Room {
            name: "kitchen".to_string(),
            devices: vec![Device::new(
                "kitchen_socket".to_string(),
                "model A".to_string(),
            )],
        };
        let mut room = Room::new("kitchen".to_string());
        let _ = room.register_new_device("socket".to_string(), "model A".to_string());

        assert_eq!(test_room, room)
    }

    #[test]
    fn turn_on_device() {
        let mut socket = Device::new("socket".to_string(), "model A".to_string());

        socket.on();
        assert_eq!(DeviceStateType::ON, socket.get_device_state())
    }

    #[test]
    fn turn_off_device() {
        let mut socket = Device {
            name: "socket".to_string(),
            state: DeviceStateType::ON,
            info: "model A".to_string(),
        };

        socket.off();
        assert_eq!(DeviceStateType::OFF, socket.get_device_state())
    }
}
