use dotenv::dotenv;
use sqlx::postgres::PgPoolOptions;
use std::{io, process, env, error::Error};
use smart_house::SmartHouse;

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
    dotenv().ok();
    let pool = PgPoolOptions::new()
      .max_connections(5)
      .connect(&env::var("DATABASE_URL").unwrap()[..])
      .await?;

    let house = SmartHouse::new("My home".to_string());

    loop {
      println!("Menu:\n\t1. Add new room\n\t2. Add new device\n\t3. Remove room by name\n\t4. Remove device by name\n\t5. Get rooms\n\t6. Get room by name\n\t7. Get report\n\t8. Exit\n");
      let mut command = String::new();
      let _ = io::stdin().read_line(&mut command);
      let command: i8 = command.trim().parse()?;

      match command {
          1 => {
            let _ = house.register_new_room(&pool);
            ()
          },
          2 => {
            todo!();
            // println!("Enter device's name:\n");
            // let mut device_name = String::new();
            // let _ = io::stdin().read_line(&mut device_name);

            // println!("Enter device's info:\n");
            // let mut device_info = String::new();
            // let _ = io::stdin().read_line(&mut device_info);

            // println!("Enter room, which should contains new device:\n");
            // let mut room_name = String::new();
            // let _ = io::stdin().read_line(&mut room_name);

            // let room = house.get_room_by_name(room_name).unwrap();
            // *room.register_new_device(device_name, device_info);
            // println!("{:?}", room);
          },
          3 => {
            todo!();
            // println!("Enter room's name:");
            // let mut room_name = String::new();
            // let _ = io::stdin().read_line(&mut room_name);

            // let _ = house.delete_room(room_name);
          },
          4 => {
            todo!();
          },
          5 => {
            todo!();
            // let rooms = house.get_rooms().unwrap();
            // println!("{:?}", rooms);
          },
          6 => {
            todo!();
          },
          7 => {
            todo!();
            // house.create_report()
          },
          8 => process::exit(0),
          _ => {
            todo!();
            // eprintln!("Command is uncorrect!");
          }
      }
    }
}
