CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE
    IF NOT EXISTS device (
        id UUID PRIMARY KEY NOT NULL DEFAULT (uuid_generate_v4()),
        name VARCHAR(255) NOT NULL UNIQUE,
        state BOOLEAN DEFAULT FALSE,
        info TEXT NOT NULL
    );

CREATE TABLE
    IF NOT EXISTS room (
        id UUID PRIMARY KEY NOT NULL DEFAULT (uuid_generate_v4()),
        device_id UUID NOT NULL,
        name VARCHAR(255) NOT NULL UNIQUE,
        CONSTRAINT fk_device FOREIGN KEY(device_id) REFERENCES device(id)
    );

CREATE TABLE
    IF NOT EXISTS house (
        id UUID PRIMARY KEY NOT NULL DEFAULT (uuid_generate_v4()),
        room_id UUID NOT NULL,
        name VARCHAR(255) NOT NULL UNIQUE,
        CONSTRAINT fk_room FOREIGN KEY(room_id) REFERENCES room(id)
    );
