use smart_house::*;

#[test]
fn integration_create_house() {
  let house = SmartHouse::new("My home".to_string());
  let mock_house = SmartHouse {
    name: "My home".to_string(),
    rooms: vec![],
  };

  assert_eq!(mock_house, house);
}

// #[test]
// fn integration_register_new_room() {
//   let mut house = SmartHouse::new("My home".to_string());
//   let kitchen = Room::new("kitchen".to_string());
//   let _ = house.register_new_room(kitchen);

//   let rooms = house.get_rooms().unwrap();
//   assert!(!rooms.is_empty());
// }

// #[test]
// fn integration_register_new_device() {
//   let mut house = SmartHouse::new("My home".to_string());
//   let mut kitchen = Room::new("kitchen".to_string());
//   let _ = kitchen
//   .register_new_device("socket1".to_string(), "Model A".to_string()).unwrap()
//   .register_new_device("socket2".to_string(), "Model A".to_string()).unwrap()
//   .register_new_device("double boiler".to_string(), "Model B".to_string());

//   let _ = house.register_new_room(kitchen);
//   let devices = house
//     .get_room_by_name("kitchen".to_string())
//     .unwrap()
//     .get_devices()
//     .unwrap();

//   assert!(!devices.is_empty());
// }